///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Joshua Lorica
// @date   2/6/21
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define YEAR_S 31556952
#define DAY_S 86400
#define HOUR_S 3600
#define MIN_S 60

int main() {

   //Need: Years, Days, Hourse, Minutes, and Seconds

   //Declare current time
   time_t now;

   //Declare time variables
   int years, days, hours, mins, secs, count;

   //Declare and fixed date
   struct tm start;
   start = *localtime(&now);
   
   start.tm_sec = 0;
   start.tm_min = 0;               
   start.tm_hour = 12;
   start.tm_mday = 7; 
   start.tm_mon = 1;
   start.tm_year = 121;

   while(1){

      //initialie current and fixed time
      time(&now);
      mktime(&start);

      //Declare and initialize seconds between current and fixed time
      count = difftime(now, mktime(&start));
  
      //Calculations
      years = count / YEAR_S;
      days = count % YEAR_S / DAY_S;
      hours = count % DAY_S / HOUR_S;
      mins = count % HOUR_S / MIN_S;
      secs = count % MIN_S;

      printf("Years: %i  Days: %i  Hours: %i  Minutes: %i  Seconds: %i\n", years, days, hours, mins, secs);
      sleep(1);
   }

   return 0;
}
